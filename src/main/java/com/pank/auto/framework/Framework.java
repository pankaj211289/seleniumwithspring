package com.pank.auto.framework;

import com.pank.auto.framework.basicActions.Actions;
import com.pank.auto.framework.driver.Driver;
import com.pank.auto.framework.reports.Report;

public interface Framework {

	Driver getDriver();
	
	Actions getActions();
	
	Report getReport();
}
