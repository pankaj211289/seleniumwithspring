package com.pank.auto.framework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.pank.auto.framework.basicActions.Actions;
import com.pank.auto.framework.driver.Driver;
import com.pank.auto.framework.reports.Report;

@Component
public class SeleniumFramework implements Framework {

	@Autowired @Qualifier("seleniumDriverImpl")
	private Driver driver;
	@Autowired @Qualifier("seleniumActions")
	private Actions actions;
	@Autowired @Qualifier("testNGReport")
	private Report report;
	
	@Override
	public Driver getDriver() {
		return driver;
	}

	@Override
	public Actions getActions() {
		return actions;
	}
	
	@Override
	public Report getReport(){
		return report;
	}
}
