package com.pank.auto.framework.basicActions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public interface Actions {

	public WebElement get(By by);
	
	public List<WebElement> getElements(By by);
	
	public void pressElement(By by);
	
	public void enterText(By by, String text);
	
	public void setCheckbox(By by, Boolean desiredState);
	
	public void setRadioButton(By by);
	
	public void selectOption(By by, String option);
	
	public WebElement waitForElement(By by);
	
	public WebElement waitForVisible(By by);
	
	public WebElement waitForEnable(By by);
	
}
