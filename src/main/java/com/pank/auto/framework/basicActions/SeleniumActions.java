package com.pank.auto.framework.basicActions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.pank.auto.framework.driver.Driver;

@Component
public class SeleniumActions implements Actions {

	@Autowired @Qualifier("seleniumDriverImpl")
	private Driver driver;
	
	private final int TIMEOUT = 30;

	@Override
	public WebElement get(By by) {
		return driver.getInstance().findElement(by);
	}

	@Override
	public List<WebElement> getElements(By by) {
		return driver.getInstance().findElements(by);
	}

	@Override
	public void pressElement(By by) {
		waitForEnable(by).click();
	}

	@Override
	public void enterText(By by, String text) {
		waitForEnable(by).sendKeys(text);
	}

	@Override
	public void setCheckbox(By by, Boolean desiredState) {
		WebElement we = waitForEnable(by);
		if(we.isSelected()!=desiredState)
			we.click();
	}

	@Override
	public void setRadioButton(By by) {
		waitForElement(by).click();
	}

	@Override
	public void selectOption(By by, String option) {
		Select select = new Select(waitForEnable(by));
		select.selectByVisibleText(option);
	}

	@Override
	public WebElement waitForElement(By by) {
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), TIMEOUT);
		return wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	@Override
	public WebElement waitForVisible(By by) {
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), TIMEOUT);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	@Override
	public WebElement waitForEnable(By by) {
		WebDriverWait wait = new WebDriverWait(driver.getInstance(), TIMEOUT);
		return wait.until(ExpectedConditions.elementToBeClickable(by));
	}
}
