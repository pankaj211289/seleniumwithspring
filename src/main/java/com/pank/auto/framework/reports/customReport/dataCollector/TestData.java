package com.pank.auto.framework.reports.customReport.dataCollector;

import java.util.List;

public class TestData {
	private String name;
	private String passed;
	private String failed;
	private String skipped;
	private String browser;
	private List<ClassData> classes;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassed() {
		return passed;
	}
	public void setPassed(String passed) {
		this.passed = passed;
	}
	public String getFailed() {
		return failed;
	}
	public void setFailed(String failed) {
		this.failed = failed;
	}
	public String getSkipped() {
		return skipped;
	}
	public void setSkipped(String skipped) {
		this.skipped = skipped;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public List<ClassData> getClasses() {
		return classes;
	}
	public void setClasses(List<ClassData> classes) {
		this.classes = classes;
	}
}
