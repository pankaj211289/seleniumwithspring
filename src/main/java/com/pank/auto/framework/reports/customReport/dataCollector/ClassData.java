package com.pank.auto.framework.reports.customReport.dataCollector;

import java.util.List;

public class ClassData {
	private String name;
	private String fullyQualifiedName;
	private List<MethodData> methods;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullyQualifiedName() {
		return fullyQualifiedName;
	}
	public void setFullyQualifiedName(String fullyQualifiedName) {
		this.fullyQualifiedName = fullyQualifiedName;
	}
	public List<MethodData> getMethods() {
		return methods;
	}
	public void setMethods(List<MethodData> methods) {
		this.methods = methods;
	}
}
