package com.pank.auto.framework.reports.customReport.createJSONUtil.readJSONFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.io.Files;

public class JSONUtil {
	
	public Object readJSONFile(String fileName){
		JSONParser jsonParser = new JSONParser();
		try {
			return jsonParser.parse(new FileReader(fileName));
		} catch (IOException e) {
			System.out.println("File not found");
			return null;
		} catch (ParseException e) {
			System.out.println("Parse Error");
			return null;
		}
	}
	
	public void copyAndCreateFile(String oldFile, String newFile){
		try {
			Files.copy(new File(oldFile), new File(newFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readAndWriteFile(String srcPath, String desPath, String... stringsToRemove){
		try {
			String fileContent = getFileText(srcPath);
			for(String stringToRemove: stringsToRemove){
				fileContent = fileContent.replaceAll(stringToRemove, "");
			}
			Files.write(fileContent, new File(desPath), Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getFileText(String filePath){
		String fileContent = null;
		try {
			fileContent = Files.toString(new File(filePath), Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileContent;
	}
	
	public void writeTextToFile(String textToAdd, File file){
		try {
			Files.write(textToAdd, file, Charset.defaultCharset());
		} catch (IOException e) {
			System.out.println("I/O exception while writing text to File");
		}
	}

	public JSONObject createNewJSONObject(){
		return new JSONObject();
	}
	
	public JSONArray createNewJSONArray(){
		return new JSONArray();
	}
	
	@SuppressWarnings("unchecked")
	public void addElementToJSONObject(JSONObject jsonObject, String key, String value){
		jsonObject.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public void addElementToJSONObject(JSONObject jsonObject, String key, JSONArray value){
		jsonObject.put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public void addElementToJSONArray(JSONArray jsonArray, String... values){
		for (String value : values) {
			jsonArray.add(value);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void addElementToJSONArray(JSONArray jsonArray, JSONObject... jObjects){
		for (JSONObject jobj : jObjects) {
			jsonArray.add(jobj);
		}
	}
}
