package com.pank.auto.framework.reports.customReport.dataCollector;

import java.util.ArrayList;
import java.util.List;

public class TestExecutionDataCollector {

	private static TestExecutionDataCollector collector = new TestExecutionDataCollector();
	private static List<SuiteData> suiteData = new ArrayList<>();
	
	// Makes it singleton
	private TestExecutionDataCollector(){}
	
	public List<SuiteData> getAllSuite(){
		return suiteData;
	}
	
	public static TestExecutionDataCollector getInstance(){
		return collector;
	}
	
	public synchronized void addSuiteData(String name){
		getSuite(name).setName(name);
	}
	
	public synchronized void addTestData(String suiteName, String testName, String passed, String failed, String skipped, String browser){
		TestData testData = getTest(suiteName, testName);
		testData.setName(testName);
		testData.setPassed(passed);
		testData.setFailed(failed);
		testData.setSkipped(skipped);
		testData.setBrowser(browser);
	}
	
	public synchronized void addClassData(String suiteName, String testName, String className, String fullName){
		ClassData classData = getClass(suiteName, testName, className);
		classData.setFullyQualifiedName(fullName);
		classData.setName(className);
	}
	
	public synchronized void addMethodData(String suiteName, String testName, String className, String methodName, String result, String log){
		MethodData methodData = getMethod(suiteName, testName, className, methodName);
		methodData.setMethodName(methodName);
		methodData.setResult(result);
		methodData.setLog(log);
	}
	
	public synchronized SuiteData getSuite(String name){
		for(SuiteData suite: suiteData){
			if(suite.getName().equals(name)){
				return suite;
			}
		}
		SuiteData suite = new SuiteData();
		suiteData.add(suite);
		suite.setName(name);
		if(suite.getTests() == null){
			suite.setTests(new ArrayList<>());
		}
		return suite;
	}
	
	public synchronized TestData getTest(String suiteName, String testName){
		SuiteData suite = getSuite(suiteName);
		for(TestData test:suite.getTests()){
			if(test.getName().equals(testName))
				return test;
		}
		TestData testData = new TestData();
		testData.setName(testName);
		if(suite.getTests() == null){
			suite.setTests(new ArrayList<>());
		}
		suite.getTests().add(testData);
		return testData;
	}
	
	public synchronized ClassData getClass(String suiteName, String testName, String className){
		TestData testData = getTest(suiteName, testName);
		if(testData.getClasses() != null){
			for(ClassData clazz: testData.getClasses()){
				if(clazz.getName().equals(className))
					return clazz;
			}
		}
		ClassData classData = new ClassData();
		classData.setName(className);
		if(testData.getClasses() == null){
			testData.setClasses(new ArrayList<>());
		}
		testData.getClasses().add(classData);
		return classData;
	}
	
	public synchronized MethodData getMethod(String suiteName, String testName, String className, String methodName){
		ClassData classData = getClass(suiteName, testName, className);
		if(classData.getMethods() != null){
			for(MethodData method: classData.getMethods()){
				if(method.getMethodName().equals(methodName)){
					return method;
				}
			}
		}
		MethodData methodData = new MethodData();
		methodData.setMethodName(methodName);
		if(classData.getMethods() == null){
			classData.setMethods(new ArrayList<>());
		}
		classData.getMethods().add(methodData);
		return methodData;
	}
}
