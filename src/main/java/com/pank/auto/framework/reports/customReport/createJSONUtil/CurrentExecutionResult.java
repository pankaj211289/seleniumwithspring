package com.pank.auto.framework.reports.customReport.createJSONUtil;

import java.io.File;

import org.json.simple.JSONObject;

import com.pank.auto.framework.reports.customReport.createJSONUtil.readJSONFile.JSONUtil;

public class CurrentExecutionResult {

	private JSONUtil jsonUtil = new JSONUtil();
	
	public static void main(String[] args){
		CurrentExecutionResult c = new CurrentExecutionResult();
		c.createExecutionResultJs("198", "12", "1");
	}
	
	public void createExecutionResultJs(String totalPassed, String totalFailed, String totalSkipped){
		JSONObject mainObj = jsonUtil.createNewJSONObject();
		jsonUtil.addElementToJSONObject(mainObj, "totalPassed", totalPassed);
		jsonUtil.addElementToJSONObject(mainObj, "totalFailed", totalFailed);
		jsonUtil.addElementToJSONObject(mainObj, "totalSkipped", totalSkipped);
		
		jsonUtil.writeTextToFile("var currentExeResults = " + mainObj.toJSONString() + ";", new File("customReport/currentExeResults.js"));
	}
	
}
