package com.pank.auto.framework.reports.customReport;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.pank.auto.framework.reports.customReport.dataCollector.TestExecutionDataCollector;

public class CustomListener extends TestListenerAdapter {
	
	private static int passed = 0;
	private static int failed = 0;
	private static int skipped = 0;
	
	@Override
	public void onTestSuccess(ITestResult result) {
		String browser = null;
		TestExecutionDataCollector testExecutionDataCollector = TestExecutionDataCollector.getInstance();
		String suiteName = result.getTestContext().getSuite().getName();
		String testName = result.getTestContext().getName();
		String fullyQualifiedName = result.getInstanceName();
		String methodName = result.getName();
		String className = fullyQualifiedName.substring(fullyQualifiedName.lastIndexOf(".") + 1);
		if(result.getTestClass().getXmlTest().getParameter("browser") != null)
			browser = result.getTestClass().getXmlTest().getParameter("browser");
		else if(System.getProperty("Browser") != null)
			browser = System.getProperty("Browser");
		else if(System.getProperty("browser") != null)
			browser = System.getProperty("browser");
		
		testExecutionDataCollector.addSuiteData(suiteName);
		testExecutionDataCollector.addTestData(suiteName, testName, ++passed +"", failed +"", skipped +"", browser);
		testExecutionDataCollector.addClassData(suiteName, testName, className, fullyQualifiedName);
		testExecutionDataCollector.addMethodData(suiteName, testName, className, methodName, "pass", "log");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		String browser = null;
		TestExecutionDataCollector testExecutionDataCollector = TestExecutionDataCollector.getInstance();
		String suiteName = result.getTestContext().getSuite().getName();
		String testName = result.getTestContext().getName();
		String fullyQualifiedName = result.getInstanceName();
		String methodName = result.getName();
		String className = fullyQualifiedName.substring(fullyQualifiedName.lastIndexOf(".") + 1);
		if(result.getTestClass().getXmlTest().getParameter("browser") != null)
			browser = result.getTestClass().getXmlTest().getParameter("browser");
		else if(System.getProperty("Browser") != null)
			browser = System.getProperty("Browser");
		else if(System.getProperty("browser") != null)
			browser = System.getProperty("browser");
		
		testExecutionDataCollector.addSuiteData(suiteName);
		testExecutionDataCollector.addTestData(suiteName, testName, passed +"", ++failed +"", skipped +"", browser);
		testExecutionDataCollector.addClassData(suiteName, testName, className, fullyQualifiedName);
		testExecutionDataCollector.addMethodData(suiteName, testName, className, methodName, "fail", "log");
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		String browser = null;
		TestExecutionDataCollector testExecutionDataCollector = TestExecutionDataCollector.getInstance();
		String suiteName = result.getTestContext().getSuite().getName();
		String testName = result.getTestContext().getName();
		String fullyQualifiedName = result.getInstanceName();
		String methodName = result.getName();
		String className = fullyQualifiedName.substring(fullyQualifiedName.lastIndexOf(".") + 1);
		if(result.getTestClass().getXmlTest().getParameter("browser") != null)
			browser = result.getTestClass().getXmlTest().getParameter("browser");
		else if(System.getProperty("Browser") != null)
			browser = System.getProperty("Browser");
		else if(System.getProperty("browser") != null)
			browser = System.getProperty("browser");
		
		testExecutionDataCollector.addSuiteData(suiteName);
		testExecutionDataCollector.addTestData(suiteName, testName, passed +"", failed +"", ++skipped +"", browser);
		testExecutionDataCollector.addClassData(suiteName, testName, className, fullyQualifiedName);
		testExecutionDataCollector.addMethodData(suiteName, testName, className, methodName, "fail", "log");
	}

}
