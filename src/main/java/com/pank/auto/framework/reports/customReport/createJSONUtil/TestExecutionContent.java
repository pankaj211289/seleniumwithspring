package com.pank.auto.framework.reports.customReport.createJSONUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.pank.auto.framework.reports.customReport.createJSONUtil.readJSONFile.JSONUtil;
import com.pank.auto.framework.reports.customReport.dataCollector.ClassData;
import com.pank.auto.framework.reports.customReport.dataCollector.MethodData;
import com.pank.auto.framework.reports.customReport.dataCollector.SuiteData;
import com.pank.auto.framework.reports.customReport.dataCollector.TestData;
import com.pank.auto.framework.reports.customReport.dataCollector.TestExecutionDataCollector;

public class TestExecutionContent {

	private JSONUtil jsonUtil = new JSONUtil();
	private String src = "customReport/testExecutionContent.js";
	private String des = "tempFiles/testExecutionContent.json";
	
	public static void main(String[] args){
		
		TestExecutionContent executionContent = new TestExecutionContent();
		executionContent.readJSAndConvertToJSON();
		
		List<String> methodName = new ArrayList<>();
		methodName.add("a");
		methodName.add("b");
		List<String> methodResult = new ArrayList<>();
		methodResult.add("pass");
		methodResult.add("pass");
		List<String> methodLogs = new ArrayList<>();
		methodLogs.add("werfgsdfghjkl");
		methodLogs.add("werfgafsdfghjkl");
		
		List<String> className = new ArrayList<>();
		className.add("Class1");
		List<String> fullName = new ArrayList<>();
		fullName.add("com.pankaj.Class1");
		List<JSONArray> methodArray = new ArrayList<>();
		methodArray.add(executionContent.addMethodElementToJSONFile(methodName, methodResult, methodLogs));
		
		JSONArray classobj = executionContent.addClassElementToJSONFile(className, fullName, methodArray);
		List<String> testName = new ArrayList<>();
		testName.add("testName1");
		List<String> passedList = new ArrayList<>();
		passedList.add("12");
		List<String> failedList = new ArrayList<>();
		failedList.add("1");
		List<String> skippedList = new ArrayList<>();
		skippedList.add("2");
		List<String> browserList = new ArrayList<>();
		browserList.add("chrome");
		List<JSONArray> classList = new ArrayList<>();
		classList.add(classobj);
		JSONArray jsonArr = executionContent.addTestElementToJSONFile(testName, passedList, failedList, skippedList, browserList, classList);
		
		List<String> suiteName = new ArrayList<>();
		List<JSONArray> arr = new ArrayList<>();
		suiteName.add("suite1");
		arr.add(jsonArr);
		
		JSONArray suite = executionContent.addSuiteElementToJSONFile(suiteName, arr);
		
		System.out.println(suite);
		
//		String jsonFileText = executionContent.jsonUtil.getFileText(executionContent.des);
//		System.out.println(jsonFileText);
		String jsFileText = "var testExecutionContent = " + suite.toString() + ";";
		executionContent.jsonUtil.writeTextToFile(jsFileText, new File(executionContent.src));
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray getSuite(){
		
		JSONArray suiteArray = jsonUtil.createNewJSONArray();
		
		TestExecutionDataCollector collector = TestExecutionDataCollector.getInstance();
		List<SuiteData> suites = collector.getAllSuite();
		
		for(SuiteData suite: suites){
			JSONObject jsonSuite = jsonUtil.createNewJSONObject();
			JSONArray testArray = jsonUtil.createNewJSONArray();
			
			jsonSuite.put("name", suite.getName());
			jsonSuite.put("tests", testArray);
			
			for(TestData test: suite.getTests()){
				JSONObject jsonTest = jsonUtil.createNewJSONObject();
				JSONArray classArray = jsonUtil.createNewJSONArray();
				
				jsonTest.put("name", test.getName());
				jsonTest.put("passed", test.getPassed());
				jsonTest.put("failed", test.getFailed());
				jsonTest.put("skipped", test.getSkipped());
				jsonTest.put("browser", test.getBrowser());
				jsonTest.put("classes", classArray);
				
				for(ClassData clazz: test.getClasses()){
					JSONObject jsonClass = jsonUtil.createNewJSONObject();
					JSONArray methodArray = jsonUtil.createNewJSONArray();
					
					jsonClass.put("name", clazz.getName());
					jsonClass.put("fullName", clazz.getFullyQualifiedName());
					jsonClass.put("methods", methodArray);
					
					for(MethodData method: clazz.getMethods()){
						JSONObject jsonMethod = jsonUtil.createNewJSONObject();
						
						jsonMethod.put("name", method.getMethodName());
						jsonMethod.put("result", method.getResult());
						jsonMethod.put("log", method.getLog());
						methodArray.add(jsonMethod);
					}
					classArray.add(jsonClass);
				}
				testArray.add(jsonTest);
			}
			suiteArray.add(jsonSuite);
		}
		return suiteArray;
	}
	
	public void readJSAndConvertToJSON(){
		jsonUtil.readAndWriteFile(src, des, "var testExecutionContent =", ";");
	}
	
	public String getSource(){
		return src;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray addSuiteElementToJSONFile(List<String> suiteName, List<JSONArray> testArray){
		JSONArray suiteArray = jsonUtil.createNewJSONArray();
		JSONObject suite;
		
		for(int i = 0; i < suiteName.size(); i++){
			suite = jsonUtil.createNewJSONObject();
			jsonUtil.addElementToJSONObject(suite, "name", suiteName.get(i));
			jsonUtil.addElementToJSONObject(suite, "tests", testArray.get(i));
			suiteArray.add(suite);
		}
		
		return suiteArray;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray addTestElementToJSONFile(List<String> testName, List<String> passedList, List<String> failedList, List<String> skippedList, List<String> browser, List<JSONArray> classesArray){
		JSONArray jsonArray = jsonUtil.createNewJSONArray();
		JSONObject jsonObject;
		
		for(int i = 0; i < testName.size(); i++){
			jsonObject = jsonUtil.createNewJSONObject();
			jsonUtil.addElementToJSONObject(jsonObject, "name", testName.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "passed", passedList.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "failed", failedList.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "skipped", skippedList.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "browser", browser.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "classes", classesArray.get(i));
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray addClassElementToJSONFile(List<String> className, List<String> fullyQualifiedName, List<JSONArray> methodsArray){
		JSONArray jsonArray = jsonUtil.createNewJSONArray();
		JSONObject jsonObject;
		
		for(int i = 0; i < className.size(); i++){
			jsonObject = jsonUtil.createNewJSONObject();
			jsonUtil.addElementToJSONObject(jsonObject, "name", className.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "fullName", fullyQualifiedName.get(i));
			jsonUtil.addElementToJSONObject(jsonObject, "methods", methodsArray.get(i));
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray addMethodElementToJSONFile(List<String> methodName, List<String> methodResult, List<String> methodLogs){
		JSONArray array = jsonUtil.createNewJSONArray();
		JSONObject method;
		
		for(int i = 0; i < methodName.size(); i++){
			method = jsonUtil.createNewJSONObject();
			jsonUtil.addElementToJSONObject(method, "name", methodName.get(i));
			jsonUtil.addElementToJSONObject(method, "result", methodResult.get(i));
			jsonUtil.addElementToJSONObject(method, "log", methodLogs.get(i));
			array.add(method);
		}
		return array;
	}
}
