package com.pank.auto.framework.reports.customReport.createJSONUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;

import com.pank.auto.framework.reports.customReport.createJSONUtil.readJSONFile.JSONUtil;

public class ExecutionDates {

	private JSONUtil jsonUtil = new JSONUtil();
	private String src = "customReport/executionDates.js";
	private String des = "tempFiles/executionDates.json";
	private int MAX_LENGTH = 10;
	
	public static void main(String[] args){
		
		
		
		// Data to be entered
		ExecutionDates executionDates = new ExecutionDates();
	}
	
	public void addData(List<String> keys, List<String> values){
		
		readJSAndConvertToJSON();
		addElementToJSONFile(des, keys, values);
		String jsonFileText = jsonUtil.getFileText(des);
		String jsFileText = "var executionDates = " + jsonFileText + ";";
		jsonUtil.writeTextToFile(jsFileText, new File(src));
	}
	
	public void readJSAndConvertToJSON(){
		jsonUtil.readAndWriteFile(src, des, "var executionDates =", ";");
	}
	
	public void addElementToJSONFile(String filePath, List<String> keys, List<String> values){
		JSONArray arr = (JSONArray)jsonUtil.readJSONFile(filePath);
		JSONObject ob = jsonUtil.createNewJSONObject();
		
		for(int i = 0; i < keys.size(); i++)
			jsonUtil.addElementToJSONObject(ob, keys.get(i), values.get(i));
		
		jsonUtil.addElementToJSONArray(arr, ob);
		removeFirstElementAfterExceeding(arr, MAX_LENGTH);
		jsonUtil.writeTextToFile(arr.toString(), new File(des));
	}
	
	private void removeFirstElementAfterExceeding(JSONArray jsonArray, int length){
		if(jsonArray.size() > length){
			jsonArray.remove(0);
		}
	}
	
}
