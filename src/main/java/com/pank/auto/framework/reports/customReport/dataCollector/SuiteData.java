package com.pank.auto.framework.reports.customReport.dataCollector;

import java.util.List;

public class SuiteData {
	private String name;
	private List<TestData> tests;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<TestData> getTests() {
		return tests;
	}
	public void setTests(List<TestData> tests) {
		this.tests = tests;
	}
}
