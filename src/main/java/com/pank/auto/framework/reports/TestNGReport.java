package com.pank.auto.framework.reports;

import java.io.File;

import org.springframework.stereotype.Component;

@Component
public class TestNGReport implements Report {

	@Override
	public String getReportLocation() {
		return new File("").getAbsolutePath() + "/test-output/index.html";
	}
}
