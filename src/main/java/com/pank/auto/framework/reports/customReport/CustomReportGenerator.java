package com.pank.auto.framework.reports.customReport;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.junit.runners.Suite;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.xml.XmlSuite;

import com.pank.auto.framework.reports.customReport.createJSONUtil.CurrentExecutionResult;
import com.pank.auto.framework.reports.customReport.createJSONUtil.ExecutionDates;
import com.pank.auto.framework.reports.customReport.createJSONUtil.TestExecutionContent;
import com.pank.auto.framework.reports.customReport.createJSONUtil.readJSONFile.JSONUtil;

public class CustomReportGenerator implements IReporter {

	private JSONUtil jsonUtil = new JSONUtil();
	private int totalPassed;
	private int totalFailed;
	private int totalSkipped;
	private Date executionDate;
	private int totalTests;
	
	@SuppressWarnings("unchecked")
	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		
		totalPassed = 0;
		totalFailed = 0;
		totalSkipped = 0;
		
		for(ISuite suite: suites){
			String suiteName = suite.getName();
			
			Map<String, ISuiteResult> suiteResults = suite.getResults();
			for(ISuiteResult suiteResult: suiteResults.values()){			
				ITestContext testContext = suiteResult.getTestContext();
				System.out.println(testContext.getName());
				ITestNGMethod[] iTestNGMethod = testContext.getAllTestMethods();
				
				for(ITestNGMethod method : iTestNGMethod){
					System.out.println(method.getMethodName());
					System.out.println(method.getTestClass().getName());
				}
				
				totalPassed += testContext.getPassedTests().getAllResults().size();
				totalFailed += testContext.getFailedTests().getAllResults().size();
				totalSkipped += testContext.getSkippedTests().getAllResults().size();
				executionDate = testContext.getEndDate();
				totalTests = totalPassed + totalFailed + totalSkipped;
				System.out.println(executionDate);
			}
		}
		
		System.out.println(totalPassed);
		System.out.println(totalFailed);
		System.out.println(totalSkipped);
		TestExecutionContent content = new TestExecutionContent();
		List<JSONArray> suiteArray = content.getSuite();
		
		TestExecutionContent executionContent = new TestExecutionContent();
		executionContent.readJSAndConvertToJSON();
		String jsFileText = "var testExecutionContent = " + suiteArray.toString() + ";";
		jsonUtil.writeTextToFile(jsFileText, new File(executionContent.getSource()));
		
		CurrentExecutionResult currentExecutionResult = new CurrentExecutionResult();
		currentExecutionResult.createExecutionResultJs(totalPassed+"", totalFailed+"", totalSkipped+"");
		
		ExecutionDates executionDates = new ExecutionDates();
		List<String> keys = new ArrayList<>();
		List<String> values = new ArrayList<>();
		keys.add("date");
		keys.add("data");
		
		String date = new SimpleDateFormat("dd/MMM/yy HH:mm:ss").format(executionDate);;
		values.add(date);
		float passPercent = ((totalPassed/(float)totalTests)*100);
		values.add(passPercent +"");
		
		executionDates.addData(keys, values);
	}
}
