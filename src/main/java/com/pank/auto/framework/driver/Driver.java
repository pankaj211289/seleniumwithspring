package com.pank.auto.framework.driver;

import org.openqa.selenium.WebDriver;

public interface Driver {

	public WebDriver getInstance();
	
	public void launchApplication(String url);
	
	public void closeApplication();
}
