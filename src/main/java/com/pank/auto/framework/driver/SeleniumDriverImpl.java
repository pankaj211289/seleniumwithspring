package com.pank.auto.framework.driver;

import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;

import com.pank.auto.framework.driver.seleniumDriver.Browser;
import com.pank.auto.framework.driver.seleniumDriver.LaunchBrowser;

@Component
public class SeleniumDriverImpl implements Driver {
	
	private LaunchBrowser launchBrowser = new LaunchBrowser();
	ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>(){
		
		@Override
		public WebDriver initialValue(){
			return init();
		}
	};
	
	private WebDriver init(){
		return launchBrowser.getDriver(Browser.CHROME);
	}
	
	@Override
	public WebDriver getInstance() {
		return driver.get();
	}
	
	@Override
	public void launchApplication(String url) {
		getInstance().get(url);
	}

	@Override
	public void closeApplication() {
		getInstance().quit();
		driver.remove();
	}
}

