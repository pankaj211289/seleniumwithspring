package com.pank.auto.framework.driver.seleniumDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class LaunchBrowser {
		
	public WebDriver getDriver(Browser browser) {
		
		if(browser == Browser.FIREFOX){
			
			return new FirefoxDriver();
		} else if(browser == Browser.CHROME){
			
			System.setProperty("webdriver.chrome.driver", "supportingBinaries/chromedriver.exe");
			return new ChromeDriver();
		} else if(browser == Browser.IE){
			
			System.setProperty("webdriver.ie.driver", "supportingBinaries/IEDriverServer.exe");
			return new InternetExplorerDriver();
		}
		
		return null;
	}
}
