package com.pank.auto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.pank.auto.framework.Framework;

@Configuration
@ComponentScan(basePackages = "com.pank.auto.framework")
public class FrameworkConfig {

	@Autowired @Qualifier("seleniumFramework")
	private Framework framework;
	
	@Bean
	public Framework framework(){
		return framework;
	}
}
