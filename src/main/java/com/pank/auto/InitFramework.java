package com.pank.auto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pank.auto.framework.Framework;


public class InitFramework {

	private static Framework myFramework;
	
	private InitFramework(){}
	
	private static void init(){
		
		@SuppressWarnings("resource")
		ApplicationContext context = new AnnotationConfigApplicationContext(FrameworkConfig.class);
		myFramework = context.getBean("framework", Framework.class);
	}
	
	public static Framework getFramework(){
		
		if(myFramework == null)
			init();
		return myFramework;
	}
}
