package com.pank.auto;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

import com.pank.auto.framework.Framework;
import com.pank.auto.framework.driver.Driver;
import com.pank.auto.framework.reports.Report;

public class BasicSetUp {

	protected Logger logger = null;
	protected Driver driver;
	protected Framework framework;
	private Report report;
	
	public BasicSetUp(){
		framework = InitFramework.getFramework();
		driver = framework.getDriver();
		report = framework.getReport();
	}
	
	@BeforeClass
	public void startLogging(){
		Class<? extends BasicSetUp> currentTestClass = getClass();
		System.out.println("############" + currentTestClass.getName());
		logger = Logger.getLogger(currentTestClass.getName());
		logger.info("Started executing class "+ currentTestClass.getName());
	}
	
	
	public void verify(String message){
		logger.log(Level.ALL, message);
	}
	
	@AfterClass
	public void cleanUp(){
		driver.closeApplication();
	}
	
	@AfterSuite
	public void closeUpLogging(){
		System.out.println(report.getReportLocation());
	}
}
