var testExecutionContent = [
		{
			name : "Suite1",
			tests : [
				{
					name  : "testName1",
					passed : "12",
					failed : "1",
					skipped : "2",
					browser : "Chrome",
					classes : [
						{
							name : "Class1",
							fullName : "com.pankaj.Class1",
							methods : [
								{
									name : "a",
									result : "pass",
									log : "ertyui"
								},
								{
									name : "b",
									result : "pass",
									log : "ertwqrefyui"
								}
							]
						},
						{
							name : "Class12",
							fullName : "com.pankaj.Class2",
							methods : [
								{
									name : "a",
									result : "fail",
									log : "eewterggcrtyui"
								},
								{
									name : "b",
									result : "pass",
									log : "ertyuqwefergerwfi"
								}
							]
						}
					]
				},
				{
					name  : "testName2",
					passed : "121",
					failed : "12",
					skipped : "21",
					browser : "Chrome",
					classes : [
						{
							name : "Class1",
							fullName : "com.pankaj.Class1",
							methods : [
								{
									name : "a",
									result : "pass",
									log : "erdvtyui"
								},
								{
									name : "b",
									result : "pass",
									log : "ertydvsfbui"
								}
							]
						},
						{
							name : "Class11",
							fullName : "com.pankaj.Class2",
							methods : [
								{
									name : "a",
									result : "fail",
									log : "ertdsgeryui"
								},
								{
									name : "b",
									result : "pass",
									log : "erewgergtyui"
								}
							]
						}
					]
				}
			]
		},
		{
			name : "Suite2",
			tests : [
				{
					name  : "testName2",
					passed : "312",
					failed : "12",
					skipped : "32",
					browser : "Chrome",
					classes : [
						{
							name : "Class1",
							fullName : "com.pankaj.Class3",
							methods : [
								{
									name : "a",
									result : "fail",
									log : "ertergetytruyui"
								},
								{
									name : "b",
									result : "fail",
									log : "ertregtytyui"
								}
							]
						},
						{
							name : "Class12",
							fullName : "com.pankaj.Class4",
							methods : [
								{
									name : "a",
									result : "pass",
									log : "ertyertyujuk,ui"
								},
								{
									name : "b",
									result : "pass",
									log : "ertretyuikyui"
								}
							]
						}
					]
				}
			]
		}
	];