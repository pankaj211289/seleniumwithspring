var app = angular.module('reportsApp', ['chart.js']);

app.controller('PieCtrl', function ($scope) {

	$scope.passed = currentExeResults.totalPassed;
	$scope.failed = currentExeResults.totalFailed;
	$scope.skipped = currentExeResults.totalSkipped;
	$scope.labels = ["Passed", "Failed", "Skipped"];
	$scope.data = [currentExeResults.totalPassed, currentExeResults.totalFailed, currentExeResults.totalSkipped];
	$scope.colours = ["#2D762D", "#FF0000", "#CCD65C"];
	$scope.options =  {
	  responsive: false,
	  maintainAspectRatio: false
	}
});

app.controller('LineCtrl', ['$scope', function($scope){

	var storedDates = [];
	var storedData = [];

	for(var i = 0; i < executionDates.length; i++){
		storedDates.push(executionDates[i].date);
	}

	for(var i = 0; i < executionDates.length; i++){
		storedData.push(executionDates[i].data);
	}

	// $scope.labels = ["1-aug-16", "2-aug-16", "3-aug-16", "4-aug-16", "5-aug-16"];
	$scope.labels = storedDates;
	$scope.series = ['Pass % '];
	$scope.data = [storedData];

	$scope.onClick = function (points, evt) {
    console.log(points, evt);
  	};
  	$scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
	$scope.options = {
		responsive: false,
	  	maintainAspectRatio: false,
		scales: {
		  yAxes: [
		    {
		      id: 'y-axis-1',
		      type: 'linear',
		      display: true,
		      position: 'left'
		    }
		  ]
		}
	};
}]);

app.controller('reportViewController', ['$scope', function($scope){

	$scope.suites = testExecutionContent;
}]);